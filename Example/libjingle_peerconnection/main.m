//
//  main.m
//  libjingle_peerconnection
//
//  Created by Rahul Behera on 07/28/2014.
//  Copyright (c) 2014 Rahul Behera. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "APPRTCAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([APPRTCAppDelegate class]));
    }
}
